# Overview
The goal is the configure the unmanaged Unity package.

A sandbox must already exist.
The git repo for the unmanaged Motvis Unity software must be installed in that sandbox, ready to be configured.

# How to log in
We now have SAML set up for CAS authentication.
Sandboxes: https://mynau--sandboxname.csNN.my.salesforce.com
Example for QA sandbox: https://mynau--qa.cs22.my.salesforce.com
Production: https://mynau.my.salesforce.com

If that doesn't work for some reason, use the older method for logging in:
test.salesforce.com
username: user@nau.edu.sandbox_name
where user@nau.edu is an email, and sandbox_name is the name of the sandbox to log in to

Enter the Setup section.

# Useful documentation
https://appexchange.salesforce.com/listingDetail?listingId=a0N30000000qFB0EAM
Open the Details tab
Download "Unity Installation Guide" (although this is version 1.10; there is a version 1.18 that Holly got somewhere and can forward to you).

# Make profiles
Manage Users > Profiles
Click "New Profile"

Clone Unity Standard User into:
Unity Community Student
Unity Community Faculty

# Make a new community
Build > Customize > Communities > Community Setup and Administration
Click "New Community"

Choose "Salesforce Tabs + Visualforce" template

Name of new community: NAU Community
URL: 
For sandboxes: sandboxname-mynau.csNN.force.com/community
Example for QA: https://qa-mynau.cs22.force.com/community

# Add members
(Community management can be found at a url like this: https://dv-mynau.cs22.force.com/community/_ui/networks/management/NetworkManagementPage?setupid=NetworkManagement OR you can find it by typing communities into the Quick Find, then select All Communities, then click Manage next to the NAU Community.)
In Community Management for the new community, select Administration > Members
Next to "Search:" select All

Add to selected profiles:

System Administrator
Chatter Free User
Chatter Moderator User
Customer Community Login User
Customer Community User
Unity Community Faculty
Unity Community Student

Remove from selected permission sets:
Unity Standard User

# Branding
In Administration > Branding

Make the header "Unity Branding Header"

Header background: #003466
Page background: #003466
Primary: #003466
Secondary: #000000
Tertiary: #FFFFFF

# Emails
In Administration > Emails

Email Address: travis.hudson@nau.edu (whover the admin is)
Footer Text: Northern Arizona University, South San Francisco Street, Flagstaff, AZ 86011, 928-523-9011

# Administration preference settings
In Administration > Preferences

Enable private messages

# Setting the Community Home page
This isn't mentioned in the Installation Guide but was important to fix a problem in DV.
In Administration > Pages

Change the Community Home setting from "Default Page" to Visualforce Page and enter or select CommunityHome.

# Create a site
Build > Develop > Sites

Enter a registration domain of "mynau" because the sandbox name will be prepended automatically, forming a url like:
http://sandboxname-mynau.cs20.force.com
For production, it will end up being mynau.force.com

Click Check Availability
Register it

Click the site label link for it
Edit

The motivisu namespace prefix isn't set up, but don't worry about it. Ignore that prefix in the Installation Instructions PDF.

Active Site Home Page:
Enter or choose CommunityHome (visualforce page)

site template:
Can find site templates with search of *site*
Use SiteTemplate named UnitySiteTemplate

url rewriter class:
CommunityURLRewriter

# Update contact page layout
Customize > Contacts > Page Layouts
Do these steps for both the Externally Mastered Layout and the Salesforce Mastered Layout:
Add Primary Role to the layout under System Information
Click Related Lists in the left pane of the Layout Properties panel
Drag Community Profiles (on the right) into the Related Lists section
Click the wrench icon in Community Profiles
Add: Community profile name, record type, and primary
Click the + sign next to Buttons
Unselect New from Standard Buttons in the same window
Click OK in that window, then Save at the top.

# Change the role values
Customize > Contacts > Fields
Click the Primary Role
Click New under Picklist Value Set
Enter "Prospective Student"
Select Salesforce Mastered
Click Save

Create > Objects
Click on Community Profile
Go to Record Types
Click New
Record Type Label: Prospective Student
Description: To be used to capture fields for a Prospective Student Community Profile
Click Active

Check under Enable for Profile:
System Administrator
Chatter Free User
Chatter Moderator User
Customer Community Login User
Customer Community User
Unity Community Faculty
Unity Community Student

Apply Community Profile Layout to all
Save

# Create fields on community profile object
Create > Objects > Community Profile

Add the following label/name/where field sets:

Alumni:LBL_About_Me:1
AlumniProfile
Alumni Community Profile (when RecordType = 'Alumni')

FacultyStaff:LBL_About_Me:1
FacultyStaffProfile
Faculty/Staff Community Profile (when RecordType = 'Faculty/Staff')

ProspectiveStudent:LBL_About_Me:1
NAU_Prospective_About_Me
Prospective Student Profile (when RecordTypes = 'Prospective')

Student:LBL_About_Me:1
Student_About_Me_1
Student Community Profile (when RecordType = 'Student')

# Update user layout
Setup > Customize > Users > Page Layouts
Click Edit next to User Layout

Add the Community Contact ID field to top of Addition Information
Add Completed Registration Date below it
Add Terms of Use below that
Delete the two Marketing cloud items

Save

# Set up images
Navigate to the home page, by selecting the Standar UI from the top right menu, then copy that url.
Delete anything after salesforce.com/

Normally, we would append "apex/motivisu__unityadmin"
But don't do that. Append "apex/unityadmin" instead.

Navigate there
In a seperate browser tab, click the Documents tab
You'll likely need to click the + symbol at the upper right of the screen to open the tab
Select Motivis Unity for folder
Click Edit next to drop down box
Set public folder access to Read/Write
Save

Click New Document
Enter a Document Name of Background Image
Select Externally Available Image checkbox
Add a description of "NAU stripe blue background"
Choose the "NAU stripe blue background" image (It's called "Background Image" and must be downloaded from an older sandbox)
Save
Copy the id from the url bar, the mix of letters and numbers at the end
The id was: 015m00000006esk

Repeat the process for another image:
Upload a new document named "Logo And Sandbox Warning" into the Motivis Unity folder
Description: For login page, warns the user that this is a sandbox and not production.
(This will also need to be downloaded independently)
Save the image id from the url
The id was: 015m00000006esp

Return to the Unity admin screen, then click Edit for Community Template Settings
Paste the background image ID into the appropriate field
Set color scheme to Dark Blue
Save

# Change text on home page for the banner image
Translation Workbench > Translation Settings
Click enable

Setup > Create > Custom Labels
(Click More Records to show a complete list)
Change LBL_Banner_Caption to "Welcome to our community"
Change LBL_Banner_Text to "Go Lumberjacks!"

# Add a twitter feed to the home page
(skip, it's not on the first sandbox)

# Choose a color scheme
(already done)

# Add Motivis Unity to the app menu
Setup > Manage Users > Profiles
Click System Administrator
Click Assigned Apps
Make Motivis Unity visible

Setup > Manage Users > Profiles
Click System Administrator
Click Object Settings

Click Community Admin
Click Edit
Change to Default On for Tab Settings

Do the same for:
Community Group Controls
Community News
Community Resources
Community Events
Community Tags
Community Themes

# Get Community Admin tab to show non-blank results
Select Motivis Unity from the app menu in the upper right hand corner
In another tab, in settup, Manage Users > Profiles
System Administrator
Edit Community Admin tab
Change the Page Layout Assignment for Community Terms to Community Terms in the drop down
Check assigned record types
In field permissions, everything is checked except Edit Access for top three

# Create terms and conditions
Go to Community Admin tab
Click New
The default of Community Terms is fine
Click continue
You are now at the New Community Admin screen
Set Terms Status to Pending
Set Published Date to today
Set Terms Body to:

You agree that:
* The information that is shared on pages, boards, and applications created or administered by University may or may not be accurate, complete, reliable or up to date.
* You read all User Content at your own risk.
* Your User Content will be accurate, will not violate any applicable law, regulation or guideline, will not violate any right of a third party including, without limitation, copyright, trademark, privacy or publicity rights, and will not cause injury to any person or entity.
* Your User Content will not contain any obscene, profane or threatening language, and will not contain software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any form of “spam.”
* We encourage you to engage in meaningful dialogue but ask when discussing or challenging each other’s views or opinions that you do so respectfully.

Click save

# Create event chatter action
Build > Create > Global Actions > Global Actions
Click New Action
Action type: Create a Record
Target object: Community Event
Standard label type: None
Label: Create Event
Name: Create_Event
Description: Create a Community Event
Create a feed item: (checked)

Click save

Add the following fields to the page:
Name
Start
End
Contact Email
Website
Description
Building
Room

Click save

# Create image library for news stories
"Creating an Image Library is a way to store some pre-selected stock images that can
be used for any new news story that is created in the Community. ... These images will display in the Image Library and will be available to all users
who create news stories so that they can select from your pre-selected stock
images rather than needing to upload a new image from their computer or from
the web for each news story that they create."
HSC: I still think this section doesn't need to be completed during the initial community configuration in order for the community to function properly. This can be done at any time and is even optional.
(skip)
If you decide to complete this section, the steps refer to the Documents tab, but it might not be shown at the top. Click the plus sign to the right of the other tabs to go to All Tabs

# Populate the image library setting
"If you will allow users to choose images from the Image Library when creating news
stories, you need to populate the Image Library Folder setting."
HSC: This designates the folder created in the section above as the place where users should get images.
(skip)

# Set up all other settings
(page 60 of instructions version 1.10; page 65 of version 1.18)
Navigate to Home in the setup section

Copy the URL but delete anything after salesforce.com/
The instructions say to append apex/motivisu__unityadmin to the end of that, but for the unmanaged package, the namespace prefix of motivisu__ isn't used, so just append apex/unityadmin.
For example: http://university.my.salesforce.com/apex/unityadmin
Navigate there.

Click edit in Community Registration
(HSC: This wasn't filled in for the portal sandbox, and this seems redundant to a setting in the Login & Registration section of the "Manage community" pages. We didn't fill this in for the unitytest or dv sandboxes either. If you decide to fill this in, follow these directions:)
First go to Setup > Manage Users > Profile, then click Unity Community Student item, then copy the last part of the url, which will look something like 00em0000000HkSW. This is the ID value for the Unity Community Student profile.
Then, in the Unity Admin screen, click edit in Community Registration
Paste the ID value for the Unity Community Student profile that you copied from the URL into the input field for Community Standard Profile ID
Click save for that section

(skip)

# Activate your community
"Note: Do not activate your Community until you have deselected the Send welcome
email checkbox within your email settings."
This is in the Manage community pages: in QuickFind, type communities then choose "All Communities". Click "Manage" next to the community name; this will take you to a new look with a left-hand nav-bar down the side. Click Administration, then under there click Emails. In the Email Templates section, UNcheck the checkbox between "Welcome New Member" and "Send welcome email", then click the Save button.

After you have activated the community, then you can re-check that box if we decide as a group to do that, so that any NEW members will get a welcome email. But  leave it unchecked until then.

# Data Setup
The only thing that needs to be done here is to create Contact records for people (team members, BAs, etc.) who need to log into the Community as a Community member but don't already have a Contact record via a feed from PeopleSoft. In production Salesforce everybody should have a Contact record already. In some sandboxes, some people have been imported on a feed. You just have to see if your person already has a contact record. Even if they do, you might need to add or change their Primary Role setting as described below.

Instructions for creating a Contact record for people (team members, BAs, etc.):
Click the Contacts tab
Click the New button.
Fill in the first and last names, their EMPLID (this MUST be their correct 7-digit emplid without the leading zeroes), for PeopleSoft Status choose "Entry Needed", for Personal Email put their NAU email address. Leave the Primary Role set to None.
Click Save.
Now click Edit, then for the Primary Role setting, choose "Student" if they just want to see how things look as a student, or "Faculty/Staff" if they want to manage things in the community like creating groups and events, etc.
Click Save again.

Now the person can access the Community URL, authenticate via CAS and their User record will be created.

(skip to the end)
